App.controller('tip_reportController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, $timeout,$state, $window, ngDialog) {
    'use strict';
    //date time picker****
    $scope.maxDate = $scope.minDate ? null : new Date();
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    // Disable weekend selection
    //$scope.disabled = function (date, mode) {
    //    //return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    //};

    //$scope.toggleMin = function () {
    //    $scope.minDate = $scope.minDate ? null : new Date();
    //};
    //$scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };
    $scope.open1 = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened1 = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['yyyy-MM-dd', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

   //============================================================================================================
   // get tip report
   // ===========================================================================================================
    $scope.create_report=function() {
        console.log("data");

        if($scope.dtInstance && $scope.dtInstance.fnDestroy()){
            $scope.dtInstance.fnDestroy();
            $('[class*=ColVis]').remove();
        }
        $scope.loading = true;

        $scope.timeZone = jstz.determine().name();
        console.log($scope.timeZone);
        $scope.startTime = moment($scope.x.startTime).format("YYYY/MM/DD");
        $scope.endTime = moment($scope.x.endTime).format("YYYY/MM/DD");
        //console.log($scope.x.startTime,$scope.x.endTime);
        //console.log($scope.startTime,$scope.endTime);


        $http({
            method: 'POST',
            url: MY_CONSTANT.url + '/api/admin/tipsReport',

            data: {
                "adminId": $cookieStore.get('obj1'),
                "accessToken": $cookieStore.get('obj'),
                "timezone": $scope.timeZone,
                "startDate": $scope.startTime,
                "endDate":  $scope.endTime
            }
        }).success(function (data) {
                console.log(data);
                $scope.loading = false;
                var dataArray = [];
                var completeList = data.data;
                //console.log(completeList);
                completeList.forEach(function (column) {
                    var d = {};
                    d.driverName=column.spFirstName +" "+column.spLastName;
                    d.email=column.email;
                    d.totalTip=column.totalTip;
                    d.totalOrderCompleted=column.totalOrderCompleted;
                    dataArray.push(d);
                });
                $scope.list = dataArray;


                console.log($scope.list);
                //var dtInstance;
                $timeout(function () {
                    if (!$.fn.dataTable) return;
                    $scope.dtInstance = $('#datatable8').dataTable({
                        'paging': true,  // Table pagination
                        'ordering': true, // Column ordering
                        'scrollX':true,
                        'info': true, // Bottom left status text
                        //'bDestroy':true,

                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ Records per page',
                            info: 'Showing page ?PAGE? of ?PAGES?',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from ?MAX? total records)'
                            //"sZeroRecords": "",
                            //"sEmptyTable": ""

                        },
                        "pageLength": 10

                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            $scope.dtInstance.fnFilter(this.value, columnInputs.index(this));});
                });
                $scope.$on('$destroy', function () {
                    $scope.dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                })

            }).error(
                function(data)
                {$scope.loading = false;
                    ngDialog.openConfirm({
                        template: 'no_data',
                        className: 'ngdialog-theme-default',
                        scope: $scope
                    });
                }
            )
    };

});